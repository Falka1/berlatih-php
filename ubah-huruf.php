<?php
function ubah_huruf($string){
// $word1=['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z']
// $word2=['b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','a']
    $newStr = '';
    for ($i=0 ; $i<strlen($string) ; $i++) {
        $newStr .= str_ireplace($string[$i],chr(ord($string[$i])+1),$string[$i]);
    }
    return $newStr.'<br>';
}

// TEST CASES
echo ubah_huruf('wow'); // xpx
echo ubah_huruf('developer'); // efwfmpqfs
echo ubah_huruf('laravel'); // mbsbwfm
echo ubah_huruf('keren'); // lfsfo
echo ubah_huruf('semangat'); // tfnbohbu

?>